import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
const Content = ({tgl , desc , onPress}) => {
    return (
        <View style={styles.container}>
        <View style={{flexDirection : 'row' , justifyContent : 'space-between'}}>
            <Text style={styles.text}>{tgl}</Text>
            <TouchableOpacity>
                <MaterialCommunityIcons name="trash-can-outline" size={25} color="black" onPress={onPress} />
            </TouchableOpacity>
        </View>
            <Text>{desc}</Text>
        </View>
    )
}

export default Content

const styles = StyleSheet.create({
    container : {
        borderWidth : 2, 
        padding : 10,
        borderColor : '#dbd9d3'
        
    },
    text  : {
        fontWeight : 'bold'
    }
})
