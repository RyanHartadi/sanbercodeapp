import React, { useState , useEffect} from 'react';
import { Alert, Button, StyleSheet, Text, TextInput, View } from 'react-native';
import Content from '../components/Content';
import Gap from '../components/Gap';
const index = () => {
    const [input , setInput] = useState('');

    const submit = () => {
        console.log(input);
        setTimeout(() => {
            Alert.alert(
                'Data Berhasil Ditambahkan',`Data ${input}`,
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: false });
        },1000)
    }
    const hapus = () => {
        Alert.alert(
            'Data ','Berhasil Dihapus',
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ],
        { cancelable: false });
    }
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Masukan Todolist</Text>
            <Gap height={20}/>
            <View style={styles.parent}>
                <TextInput style={styles.txtinput} placeholder="Masukkan TodoList" value={input} onChangeText={(value) => setInput(value)}/>
                <Button title="Tambahkan" style={styles.btn} onPress={submit}/>
            </View>
            <Gap height={50}/>
            <Content tgl="9/6/2020" desc="Membuat Login Screens" onPress={hapus}/>
            <Gap height={20}/>
            <Content tgl="9/6/2020" desc="Membuat Home Page" onPress={hapus}/>
          
        </View>
    )
}

export default index

const styles = StyleSheet.create({
    title : {
        fontSize:20,
        fontWeight : 'bold'
    },
    container : {
        padding : 20,
        paddingTop:100,
        flex:1
    },
    txtinput :{ height: 40, borderColor: 'gray', borderWidth: 1 ,width : '69%' , paddingHorizontal : 10},
    parent : {
        flexDirection : 'row',
        justifyContent :'space-between'
    },
    btn : {
        width : 100
    }
})
