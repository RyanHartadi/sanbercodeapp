import React from 'react'
import { StyleSheet, Text, SafeAreaView, View ,Image } from 'react-native';
import { Dimensions } from 'react-native';
import GeneralStatusBarColor from '../components/GeneralStatusBarColor';
import Gap from '../components/Gap';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Octicons from 'react-native-vector-icons/Octicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
const index = () => {
    return (
        <View style={styles.views}>
            <GeneralStatusBarColor backgroundColor="#4287f5" barStyle="light-content"/>
            <View style={styles.parent}>
                <Text style={styles.title}>Account</Text>
            </View>
            <View style={styles.parent2}>
                <Gap height={20}/>
                <View style={styles.flex}>  
                    <Image source={{ uri:"https://i1.sndcdn.com/avatars-000290236549-go76yz-t500x500.jpg" }} style={styles.image} />
                    <Text style={styles.text}>Ryan Hartadi</Text>    
                </View>
                <Gap height={20}/>
            </View>
            <View style={{height : 1 , backgroundColor : '#cfcdc8'}}></View>

            <View style={styles.parent2}>
                <View style={styles.flex}>
                    <AntDesign  name="wallet" size={25} color="black" />
                        <Text style={styles.text2}>Saldo</Text>
                        <View style={{alignItems : 'flex-end' , flex:1}}>  
                            <Text style={styles.text2}>Rp.120.000.000</Text>
                        </View>
                </View>
            </View>
            <View style={{height : 1 , backgroundColor : '#cfcdc8'}}></View>
            {/* Settings */}
            <View style={styles.parent2}>
                <View style={styles.flex}>
                    <AntDesign  name="setting" size={25} color="black" />
                    <Text style={styles.text2}>Pengaturan</Text>
                </View> 
            </View>
            <View style={{height : 1 , backgroundColor : '#cfcdc8'}}></View>
             {/* Bantuan */}
            <View style={styles.parent2}>
                <View style={styles.flex}>
                    <Octicons  name="question" size={25} color="black" />
                    <Text style={styles.text2}>Bantuan</Text>    
                </View>
            </View>
            <View style={{height : 1 , backgroundColor : '#cfcdc8'}}></View>
           
            <View style={styles.parent2}>
                <View style={styles.flex}>
                        <MaterialCommunityIcons  name="text-box-multiple" size={25} color="black" />
                        <Text style={styles.text2}>Syarat & Ketentuan</Text>        
                </View>
            </View>
            <View style={{height : 1 , backgroundColor : '#cfcdc8'}}></View>
            
            <View style={styles.parent2}>
                <View style={styles.flex}>
                    <AntDesign  name="logout" size={25} color="black" />
                    <Text style={styles.text2}>Keluar</Text>  
                </View>
                <Gap height={20}/>
            </View>
        </View>
     
    )
}

export default index
const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
    views: {
      backgroundColor : '#4287f5',
      height : windowHeight*0.12,
    },
    flex: {
        flexDirection : 'row'
    },
    parent: {
        padding : 20,
    },
    parent2 :{ 
        padding : 20,
    },
    title : {
        color : 'white',
        fontSize : 20
    },
    image: {
        width: 75,
        height: 75,
        borderRadius: 75 / 2,
    },
    text: {
        textAlign: 'center',
        textAlignVertical: "center",
        marginLeft : windowWidth * 0.1,
        fontSize : 20,
        fontWeight : 'bold'
    },
    text2 : {
        fontSize : 18,
        marginLeft : windowWidth * 0.1,
        fontWeight : '300'
    }
  });
  
