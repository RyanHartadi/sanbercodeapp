import React , {useState , createContext} from 'react'
import TodoList from './TodoList'

export const RootContext = createContext();

const Context = () => {
    
    const [input , setInput] = useState('')
    const [todos , setTodos] = useState([])

    handleChangeInput = (value) => {
        setInput(value)
    }
    addTodo = () => {
        const day = new Date().getDate();
        const month = new Date().getMonth();
        const year = new Date().getFullYear();
        const today = `${day}/${month}/${year}`
        setTodos([...todos , {title : input , date : today}])
        setInput('')
    }
    deleteTodo = (index) => {
        setTodos(todos.splice(index,1));
        console.log('Delete To Do')
    }
    return (
        <RootContext.Provider value={{
            input, 
            todos,
            handleChangeInput,
            addTodo,
            deleteTodo
        }}>
            <TodoList /> 
        </RootContext.Provider>
    )
}

export default Context
 
