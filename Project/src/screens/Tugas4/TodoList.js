import React, { useContext } from 'react';
import { FlatList, StatusBar, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { RootContext } from './index';
import Content from '../components/Content'
import Gap from '../components/Gap'

const TodoList = () => {
    const state = useContext(RootContext);

    const renderItem = ({item , index}) => {
        // console.log(index);
        return (
            <View style={styles.list}>
                <View style={{flex:1 , flexDirection : 'row' , justifyContent: 'space-between'}}>
                    <Text>{item.date}</Text>
                    <TouchableOpacity>
                        <Icon name="delete" size={25} onPress={() => state.deleteTodo(index)}/>
                    </TouchableOpacity>
                </View>
                <Text>{item.title}</Text>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="blue"/>
            <View>
                    <Text>Masukkan Todolist</Text>
                    <Gap height={20}/>
                    <View style={styles.parent}>
                        <TextInput style={styles.input} value={state.input} placeholder="Input Here" onChangeText={(value) => handleChangeInput(value)} />
                        <TouchableOpacity style={styles.btn} onPress={() => state.addTodo()}>
                            <Ionicons name="add" size={25}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style = {styles.listContainer}>
                <Gap height={20}/>
                    <FlatList 
                        data={state.todos}
                        renderItem={renderItem}
                    />
            </View>
        </View>
    )
}

export default TodoList

const styles = StyleSheet.create({
   container : {
        padding : 20,
        paddingTop:20,
        flex:1
   },
   parent: {
        flexDirection : 'row',
        justifyContent :'space-between'
   },
   input: {
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 0.9 ,
        width : '80%' , 
        paddingHorizontal : 10
   },
   btn : {
       backgroundColor : '#72a1ed',
       width : 35,
       justifyContent : 'center',
       alignItems : 'center',
   },
   list: {
        borderWidth : 2, 
        padding : 10,
        borderColor : '#dbd9d3'
   }
})
